package scala

import org.junit.{Assert, Test}

class CollectionTest {

  @Test def mapToMakeDouble() {
    def collection = new Collection()

    def double(x: Int): Int = x * 2

    val list = List(9, 11, 13)

    val doubled = collection.map(list, double)
    Assert.assertEquals(18, doubled.head)
    Assert.assertEquals(22, doubled.apply(1))
  }

  @Test def mapWithString() {
    def collection = new Collection()

    def addStr(x: String): String = x + "World!"

    val list = List("Hello ", "Hi ")

    val doubled = collection.map(list, addStr)
    Assert.assertEquals("Hello World!", doubled.head)
    Assert.assertEquals("Hi World!", doubled.apply(1))
  }

  @Test def filterToGetEven() {
    def collection = new Collection()

    def even(x: Int): Boolean = x % 2 == 0

    val list = List(10, 11, 12)

    val evenList = collection.filter(list, even)
    Assert.assertEquals(2, evenList.length)
    Assert.assertEquals(10, evenList.head)
  }

  @Test def filterToGetHelloWorld() {
    def collection = new Collection()

    def helloWorld(x: String): Boolean = x == "Hello world!"

    val list = List("Hello world!", "Hello Universe!")

    val string = collection.filter(list, helloWorld)
    Assert.assertEquals(1, string.length)
    Assert.assertEquals("Hello world!", string.head)
  }

  @Test def reduceToSum() {
    def collection = new Collection()

    def sum(a: Int, b: Int): Int = a + b

    def initialVal = 0

    val list = List(5, 10, 15)

    val total = collection.reduce(list, sum, initialVal)
    Assert.assertEquals(30, total)
  }

  @Test def reduceToSumWithoutInitialValue() {
    def collection = new Collection()

    def sum(a: Int, b: Int): Int = a + b

    val list = List(5, 10, 15)

    val total = collection.reduce(list, sum)
    Assert.assertEquals(30, total)
  }

  @Test def reduceToSumWithStringAndWithoutInitialValue() {
    def collection = new Collection()

    def concat(a: String, b: String): String = a.concat(b)

    val list = List("Hello ", "World", "!!!")

    val result = collection.reduce(list, concat)
    Assert.assertEquals("Hello World!!!", result)
  }

  @Test def takeForInteger() {
    def collection = new Collection()

    val list = List(9, 11, 13, 15)

    val result = collection.take(list, 2)
    Assert.assertEquals(2, result.length)
    Assert.assertEquals(9, result.head)
    Assert.assertEquals(11, result.apply(1))
  }

  @Test def takeWhileForInteger() {
    def collection = new Collection()

    val list = List(0, 1, 2, 3)

    def takeLessThanOne(x: Int): Boolean = x <= 1

    val result = collection.takeWhile(list, takeLessThanOne)
    Assert.assertEquals(2, result.length)
    Assert.assertEquals(0, result.head)
    Assert.assertEquals(1, result.apply(1))
  }

  @Test def dropForInteger() {
    def collection = new Collection()

    val list = List(9, 11, 13, 15)

    val result = collection.drop(list, 2)
    Assert.assertEquals(2, result.length)
    Assert.assertEquals(13, result.head)
    Assert.assertEquals(15, result.apply(1))
  }

  @Test def dropWhileForInteger() {
    def collection = new Collection()

    val list = List(0, 1, 2, 3, 4)

    def dropLessThanOne(x: Int): Boolean = x <= 1

    val result = collection.dropWhile(list, dropLessThanOne)
    Assert.assertEquals(3, result.length)
    Assert.assertEquals(2, result.head)
    Assert.assertEquals(3, result.apply(1))
    Assert.assertEquals(4, result.apply(2))
  }

  @Test def zipToMargeTwoArrayWhenBothAreEqual() {
    def collection = new Collection()

    val listA = List(1, 2)
    val listB = List(3, 4)

    val result = collection.zip(listA, listB)
    Assert.assertEquals(2, result.length)
    Assert.assertEquals((1, 3), result.head)
    Assert.assertEquals((2, 4), result.apply(1))
  }

  @Test def zipToMargeTwoArrayWhenFirstArrayIsBigger() {
    def collection = new Collection()

    val listA = List(1, 2, 10)
    val listB = List(3, 4)

    val result = collection.zip(listA, listB)
    Assert.assertEquals(2, result.length)
    Assert.assertEquals((1, 3), result.head)
    Assert.assertEquals((2, 4), result.apply(1))
  }

  @Test def zipToMargeTwoArrayWhenSecondArrayIsBigger() {
    def collection = new Collection()

    val listA = List(1, 2, 10)
    val listB = List(3, 4, 11, 12)

    val result = collection.zip(listA, listB)
    Assert.assertEquals(3, result.length)
    Assert.assertEquals((1, 3), result.head)
    Assert.assertEquals((2, 4), result.apply(1))
    Assert.assertEquals((10, 11), result.apply(2))
  }

  @Test def zipWithToMargeTwoArrayForSameLengthArray() {
    def collection = new Collection()

    val listA = List(1, 2)
    val listB = List(3, 4)

    def add(a: Int, b: Int): Int = a + b

    val result = collection.zipWith(add, listA, listB)
    Assert.assertEquals(2, result.length)
    Assert.assertEquals(4, result.head)
    Assert.assertEquals(6, result.apply(1))
  }

  @Test def zipWithToMargeTwoArrayFirstArrayIsBig() {
    def collection = new Collection()

    val listA = List(1, 2, 5)
    val listB = List(3, 4)

    def add(a: Int, b: Int): Int = a + b

    val result = collection.zipWith(add, listA, listB)
    Assert.assertEquals(2, result.length)
    Assert.assertEquals(4, result.head)
    Assert.assertEquals(6, result.apply(1))
  }

  @Test def zipWithToMargeTwoArrayLastArrayIsBig() {
    def collection = new Collection()

    val listA = List(1, 2)
    val listB = List(3, 4, 5)

    def add(a: Int, b: Int): Int = a + b

    val result = collection.zipWith(add, listA, listB)
    Assert.assertEquals(2, result.length)
    Assert.assertEquals(4, result.head)
    Assert.assertEquals(6, result.apply(1))
  }
}