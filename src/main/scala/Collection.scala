package scala

class Collection {
  def map[A](list: List[A], function: (A => A)): List[A] = list match {
    case List() => list
    case head :: tail => function(head) :: map(tail, function)
  }

  def filter[A](list: List[A], function: (A => Boolean)): List[A] = list match {
    case Nil => list
    case head :: tail if function(head) => head :: filter(tail, function)
    case head :: tail if !function(head) => filter(tail, function)
  }

  def reduce[A](list: List[A], function: ((A, A) => A), value: A): A = list match {
    case Nil => value
    case head :: tail => function(head, reduce(tail, function, value))
  }

  def reduce[A](list: List[A], function: ((A, A) => A)): A = list match {
    case head :: Nil => head
    case head :: tail => function(head, reduce(tail, function))
  }

  def take[A](list: List[A], range: Int): List[A] = list match {
    case Nil => list
    case _ :: _ if range == 0 => List()
    case head :: tail => head :: take(tail, range - 1)
  }

  def takeWhile[A](list: List[A], function: (A => Boolean)): List[A] = list match {
    case Nil => list
    case head :: tail if function(head) => head :: takeWhile(tail, function)
    case head :: tail if !function(head) => takeWhile(tail, function)
  }

  def drop[A](list: List[A], range: Int): List[A] = list match {
    case Nil => list
    case _ if range == 0 => list
    case _ :: tail => drop(tail, range - 1)
  }

  def dropWhile[A](list: List[A], function: (A => Boolean)): List[A] = list match {
    case Nil => List()
    case head :: tail if function(head) => dropWhile(tail, function)
    case head :: tail if !function(head) => head :: dropWhile(tail, function)
  }

  def zip[A](listA: List[A], listB: List[A]): List[Any] = (listA, listB) match {
    case (_, Nil) | (Nil, _) => List()
    case (h1 :: t1, h2 :: t2) => (h1, h2) :: zip(t1, t2)
  }

  def zipWith[A](function: ((A, A) => A), listA: List[A], listB: List[A]): List[A] = (listA, listB) match {
    case (_, Nil) | (Nil, _) => List()
    case (h1 :: t1, h2 :: t2) => function(h1, h2) :: zipWith(function, t1, t2)
  }

}


